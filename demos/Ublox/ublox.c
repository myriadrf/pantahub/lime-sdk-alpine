/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// gcc ublox.c -lm -O3 -Wall -o ublox
// sudo ./ublox "/dev/ttyAMA0"
// get occasional segmentation faults
// ./ublox should work, but gives segmentation fault
//
// LimeNet Micro
//
// sudo usermod -a -G dialout $USER
// sudo usermod -a -G tty $USER
// getent group dialout # to confirm # check if it worked
// sudo raspi-config
// Disable serial login, enable serial interface
// sudo nano /boot/config.txt
// # UART to UBLOX
// gpio=32-33=a3
// gpio=14-15=ip
// reboot
// # test
// cat /dev/ttyAMA0 # should give an endless display of NMEA messages
// minicom -D /dev/ttyAMA0  -8 -b 9600# should give an endless display of NMEA messages
//
// Things to do
// convert frac minutes into seconds for long lat
// time stamp graphs
// add GPS location information
// fix segmentation faults
// convert to standard makefile format

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // pipes
#include <sys/types.h> // pipes
//#include <sys/stat.h> // mkdir
#include <fcntl.h> // file control
//#include <termios.h> // POSIX terminal
#include <math.h>

float gpsTime[3]; // 0 hr, 1 min, 2 sec
char gpsStatus='N';
short gpsLatDeg=0; // deg
float gpsLatMin=0; // min
char gpsLatRef=' ';
short gpsLngDeg=0; // deg
float gpsLngMin=0; // min
char gpsLngRef=' ';
float gpsAlt=0; // m
float gpsSpeedN=0; // knots
float gpsTrackT=0; // heading relative to true north
float gpsSpeedK=0; // km/h
float gpsTrackM=0; // heading relative to magnetic north
int gpsDate[3]; // 0 day, 1 mnth, 2 yr
unsigned char gpsPages; // number of pages GSV uses
unsigned char gpsPage; // current page of GSV
unsigned char gpsPerPage[6]; // number satellites in each page
unsigned char gpsSat[6][6]; // list of satellites in each page

int nSats;
int nSatsInView;
char satSNR[63];
short satLocEle[63]; // elevation 
short satLocAzi[63]; // azimuth

void ReadNMEAtraffic( char *gpsPath );
char* ReadTil( char *buffer, char word[] );
void ReadSubWord(char word[],char subWord[],int pos,int len);
void ReadGPGGA( char *buffer );
void ReadGPGSV( char *buffer );
void ReadGPGLL( char *buffer );
void ReadGPRMC( char *buffer );
void ReadGPTXT( char *buffer );
void ReadGPVTG( char *buffer );

void GNUplotSatSNR( void );
void GNUplotSatLoc( void );

int main( int argc, char *argv[] )
{
	char gpsPath[20]="/dev/ttyAMA0";
	if( argc>=1 ) // can be ublox "/ttyAMA0", argc/argv strips out quotes
		strcpy( gpsPath,argv[1] );
	ReadNMEAtraffic( gpsPath );
	return(1);
}

void ReadNMEAtraffic( char *gpsPath )
{
	int ci=0;
	int cj=0;
	int ck=0;
	char buf[256];
	size_t size;
	char byte='\0';
	char header[10];
	int fd=0;
	if( (fd=open(gpsPath, O_RDWR | O_NOCTTY | O_NDELAY ))<0 )
	{
		printf("Error - cannot open Ublox GPS %s\n",gpsPath );
		exit(1);
	}
	fcntl(fd,F_SETFL,0);
	for(ci=0; ci<150;ci++)
	{
		ck=0;
		byte='\0';
		while(byte!='\n')
		{
			while( (size=read(fd,&byte,1))<1 ); // wait til ready
			buf[ck++]=byte;
		}
		ck--;
		byte='\0';
		buf[ck]='\0';
		if( ck>0 ) // ignore empty lines, seems to send \n\n
		{
			for(cj=0;cj<6;cj++)
				header[cj]=buf[cj];
			header[6]='\0';
			if(strcmp(header,"$GPGGA")==0) // yes
				ReadGPGGA( buf+7 );
			if(strcmp(header,"$GPGSV")==0) // yes
				ReadGPGSV( buf+7 );
			if(strcmp(header,"$GPGLL")==0) // yes
				ReadGPGLL( buf+7 );
//			if(strcmp(header,"$GPTXT")==0) // yes - not NMEA?
//				ReadGPTXT( buf+7 );
//			if(strcmp(header,"$GPGSA")==0) // yes - list of sats for fix
//				ReadGPGSA( buf+7 );
			if(strcmp(header,"$GPRMC")==0) // yes - nav info
				ReadGPRMC( buf+7 );
			if(strcmp(header,"$GPVTG")==0) // yes - heading speed 
				ReadGPVTG( buf+7 );
		}
	}
	GNUplotSatSNR();
	GNUplotSatLoc();
	close(fd);
}

char* ReadTil( char buffer[], char word[] )
{ // read til comma, newline or /0, occasionally get ,, * is start of checksum
	unsigned char ci=0;
	char *buf=buffer;
	word[0]='\0';
	while( ((*buf)!=',') && ((*buf)!='\n') && ((*buf)!='\0') && ((*buf)!='*') )
		word[ci++]=(*buf++);
	word[ci]='\0';
	if( ((*buf)=='\n') || ((*buf)=='\0') || ((*buf)=='*') )
		buf=NULL;
	else
		buf++;
	return(buf);
}

void ReadSubWord(char word[],char subWord[],int pos,int len)
{ // read len chars, or until end of string
	int ci=0;
	while( (ci<len) && (word[pos+ci]!='\0') )
	{
		subWord[ci]=word[pos+ci];
		ci++;
	}
	subWord[ci]='\0';
}

void ReadGPGGA( char *buffer )
{ // GPGGA,UTC hr min sec,Lat DD.M,N,Long DDD.M,W,Quality 1-4,Nsats,HDOP,Altitude,M/F,Geoid sep,M,age,
	char *buf=buffer;
	char word[20];
	char subWord[20];
	if( buf!=NULL )
	{ // hh mm ss.ss
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsTime[0]=atof(subWord);
		ReadSubWord(word,subWord,2,2);
		gpsTime[1]=atof(subWord);
		ReadSubWord(word,subWord,4,5);
		gpsTime[2]=atof(subWord);
	}
	if( buf!=NULL )
	{ // latitude
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLatDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLatMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // N or S
		buf=ReadTil( buf, word );
		gpsLatRef=word[0];
	}
	if( buf!=NULL )
	{ // long
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLngDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLngMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // E or W
		buf=ReadTil( buf, word );
		gpsLngRef=word[0];
	}
	if( buf!=NULL )
	{ // number of sats
		buf=ReadTil( buf, word );
		nSats=atoi(word);
	}
	if( buf!=NULL )
	{ // precision
		buf=ReadTil( buf, word );
//		printf("%s\n",word );
	}
	if( buf!=NULL )
	{ // altitude
		buf=ReadTil( buf, word );
		gpsAlt=atof(word);
	}
	printf("GPGGA %.0f:%.0f:%.2f UTC ",gpsTime[0],gpsTime[1],gpsTime[2]);
	printf("%io %f' %c %io %.2f' %c %.2fm\n",gpsLatDeg, gpsLatMin, gpsLatRef, gpsLngDeg, gpsLngMin, gpsLngRef, gpsAlt );
}

void ReadGPGSV( char *buffer )
{ // note last entry truncated if SNR=0
	char *buf=buffer;
	char word[20];
	unsigned char ci=0;
	unsigned char satPRN=0;
	unsigned char valid=0;
	if( buf!=NULL )
	{ // number of pages
		buf=ReadTil( buf,word );
		gpsPages=atoi(word);
	}
	if( buf!=NULL )
	{ // page number
		buf=ReadTil( buf,word );
		gpsPage=atoi(word);
	}
	if( buf!=NULL )
	{ // number of sats in view
		buf=ReadTil( buf,word );
		nSatsInView=atoi(word);
	}
	printf("GPGSV %i/%i %i\n",gpsPage,gpsPages,nSatsInView);
	gpsPerPage[gpsPage]=0;
	while( buf!=NULL )
	{ // msg numb, 
		if( buf!=NULL )
		{ // sat PRN
			buf=ReadTil( buf,word );
			satPRN=atoi(word);
			gpsSat[gpsPage][gpsPerPage[gpsPage]]=satPRN;
		}
		if( buf!=NULL )
		{ // elevation degrees
			buf=ReadTil( buf,word );
			satLocEle[satPRN]=atoi(word);
		}
		if( buf!=NULL )
		{ // azimuth degrees to true
			buf=ReadTil( buf,word );
			satLocAzi[satPRN]=atoi(word);
			valid=1;
		}
		if( buf!=NULL )
		{ // SNR in dB
			buf=ReadTil( buf,word );
			satSNR[satPRN]=atoi(word);
		}
		else
			satSNR[satPRN]=0;
		if( valid==1 )
			gpsPerPage[gpsPage]++;
	}
	for( ci=0; ci<gpsPerPage[gpsPage]; ci++ )
		printf("PRN=%i Azi=%i Ele=%i SNR=%i\n",
gpsSat[gpsPage][ci],satLocAzi[gpsSat[gpsPage][ci]],
satLocEle[gpsSat[gpsPage][ci]],satSNR[gpsSat[gpsPage][ci]] );
}

void ReadGPGLL( char *buffer )
{ // lat, N/S, long, E/W, Time UTC, Valid
	char *buf=buffer;
	char word[20];
	char subWord[20];
	if( buf!=NULL )
	{ // latitude
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLatDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLatMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // N or S
		buf=ReadTil( buf, word );
		gpsLatRef=word[0];
	}
	if( buf!=NULL )
	{ // long
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLngDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLngMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // E or W
		buf=ReadTil( buf, word );
		gpsLngRef=word[0];
	}
	if( buf!=NULL )
	{ // hh mm ss.ss
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsTime[0]=atof(subWord);
		ReadSubWord(word,subWord,2,2);
		gpsTime[1]=atof(subWord);
		ReadSubWord(word,subWord,4,5);
		gpsTime[2]=atof(subWord);
	}
	if( buf!=NULL )
	{ // staus
		buf=ReadTil( buf, word );
		gpsStatus=word[0];
	}
	printf("GPGLL %.0f:%.0f:%.2f UTC ",gpsTime[0],gpsTime[1],gpsTime[2]);
	printf("%io %.2f' %c %io %.2f' %c %c\n",gpsLatDeg,gpsLatMin,gpsLatRef, gpsLngDeg,gpsLngMin,gpsLngRef,gpsStatus);
}

void ReadGPRMC( char *buffer )
{ // time UTC, status, lat, long, speed, track, date, 
	char *buf=buffer;
	char word[20];
	char subWord[20];
	if( buf!=NULL )
	{ // hh mm ss.ss
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsTime[0]=atof(subWord);
		ReadSubWord(word,subWord,2,2);
		gpsTime[1]=atof(subWord);
		ReadSubWord(word,subWord,4,5);
		gpsTime[2]=atof(subWord);
	}
	if( buf!=NULL )
	{ // hh mm ss.ss
		buf=ReadTil( buf, word );
		if( word[0]=='V' )
			printf("WARNING\n");
	}
	if( buf!=NULL )
	{ // latitude
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLatDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLatMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // N or S
		buf=ReadTil( buf, word );
		gpsLatRef=word[0];
	}
	if( buf!=NULL )
	{ // long
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsLngDeg=atoi(subWord);
		ReadSubWord(word,subWord,2,10);
		gpsLngMin=atof(subWord);
	}
	if( buf!=NULL )
	{ // E or W
		buf=ReadTil( buf, word );
		gpsLngRef=word[0];
	}
	if( buf!=NULL )
	{ // speed
		buf=ReadTil( buf, word );
		gpsSpeedK=atof(word); // knots
	}
	if( buf!=NULL )
	{ // track
		buf=ReadTil( buf, word );
		gpsTrackT=atof(word);
	}
	if( buf!=NULL )
	{ // data 
		buf=ReadTil( buf, word );
		ReadSubWord(word,subWord,0,2);
		gpsDate[0]=atoi(subWord);
		ReadSubWord(word,subWord,2,2);
		gpsDate[1]=atoi(subWord);
		ReadSubWord(word,subWord,4,2);
		gpsDate[2]=atoi(subWord);
	}
	printf("GPRMC %.0f:%.0f:%.2f UTC ",gpsTime[0],gpsTime[1],gpsTime[2]);
	printf("%i/%i/%i UK ",gpsDate[0],gpsDate[1],gpsDate[2]);
	printf("%io %.3f'%c %io %.3f'%c ",gpsLatDeg,gpsLatMin,gpsLatRef, gpsLngDeg,gpsLngMin,gpsLngRef);
	printf("vel=%.1fkt Hdg=%.3foT\n",gpsSpeedK,gpsTrackT);
}

void ReadGPTXT( char *buffer )
{
	char *buf=buffer;
	char word[256];
	char val=0;
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
		val=atoi(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
		val=atoi(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
		val=atoi(word);
	}
//	if( val==1 ) // invalid data
//	{
//		buf=ReadTil( buf, word );
//		printf("TXT: %s\n",word);
//	}
	if( val==2 ) // valid data
	{
		buf=ReadTil( buf, word );
		printf("GPTXT: %s\n",word);
	}
}

void ReadGPVTG( char *buffer )
{
	char *buf=buffer;
	char word[20];
	if( buf!=NULL )
	{ // track, T (rel to true N)
		buf=ReadTil( buf, word );
		gpsTrackT=atof(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
//		word[0]; //==T
	}
	if( buf!=NULL )
	{ // track, M (rel to magnetic N)
		buf=ReadTil( buf, word );
		gpsTrackM=atof(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
//		word[0]; //==M
	}
	if( buf!=NULL )
	{ // speed, N (knots)
		buf=ReadTil( buf, word );
		gpsSpeedN=atof(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
//		word[0]; //==N
	}
	if( buf!=NULL )
	{ // speed, K (km/h)
		buf=ReadTil( buf, word );
		gpsSpeedK=atof(word);
	}
	if( buf!=NULL )
	{ 
		buf=ReadTil( buf, word );
//		word[0]; //==K
	}
	printf("GPVTG: %.1f o TN %.1f o MN %.3fkt %.3fkm/h\n", gpsTrackT,gpsTrackM,gpsSpeedN,gpsSpeedK);
}

void GNUplotSatSNR( void )
{
	unsigned int ci,cj;
	FILE *GpPipe;
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"./output/gpsSNR.gif\"\n");
	fprintf(GpPipe,"set grid\n");
	fprintf(GpPipe,"set title \"SNR vs PRS\"\n");
	fprintf(GpPipe,"set ylabel \"Sat PRS\"\n");
	fprintf(GpPipe,"set ylabel \"SNR dB\"\n");
	fprintf(GpPipe,"set xrange [0:37]\n");
	fprintf(GpPipe,"set yrange [0:50]\n");
	fprintf(GpPipe,"set boxwidth 1.0\n");
	fprintf(GpPipe,"set style fill solid 0.4 border\n");
	fprintf(GpPipe,"set style line 1 lt 2 lc rgb \"green\" lw 1\n");
	fprintf(GpPipe,"set style line 2 lt 2 lc rgb \"red\" lw 1\n");
	fprintf(GpPipe,"set style line 3 lt 2 lc rgb \"blue\" lw 1\n");
	fprintf(GpPipe,"plot \"-\" using 1:2 with boxes ls 1 title \"SNR dB\"\n");
	for(ci=1;ci<=gpsPages;++ci)
	{
		for(cj=0;cj<gpsPerPage[ci];++cj)
			fprintf(GpPipe,"%i %i\n",gpsSat[ci][cj],satSNR[gpsSat[ci][cj]]);
	}
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!
	
}

void GNUplotSatLoc( void )
{
	unsigned int ci,cj;
//	float deg2rad=3.141592/180;
	FILE *GpPipe;
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"./output/gpsLoc.gif\"\n");
	fprintf(GpPipe,"set polar\n");
	fprintf(GpPipe,"set size square\n");
	fprintf(GpPipe,"set angle degrees\n");
	fprintf(GpPipe,"set style line 10 lt 1 lc 0 lw 0.3\n");
	fprintf(GpPipe,"set grid polar 30\n");
	fprintf(GpPipe,"set grid ls 10\n");
	fprintf(GpPipe,"set xrange [-90:90]\n");
	fprintf(GpPipe,"set yrange [-90:90]\n");
	fprintf(GpPipe,"set palette defined (0 \"red\",10 \"orange\",20 \"yellow\", 30 \"green\", 40 \"blue\")\n");
	fprintf(GpPipe,"plot \"-\" using 1:2:3 t \"GPS Sat Loc\" with points pointtype 7 pointsize 3 palette, \"\" using 1:2:4 notitle with labels offset 1,1 lc \"black\"\n"); // two sets of using, needs two sets of graphs!
	for(ci=1;ci<=gpsPages;++ci)
	{
		for(cj=0;cj<gpsPerPage[ci];++cj)
			if( satSNR[gpsSat[ci][cj]]>0 )
			{
				fprintf(GpPipe,"%i %i %i %i",90-satLocAzi[gpsSat[ci][cj]],90-satLocEle[gpsSat[ci][cj]],satSNR[gpsSat[ci][cj]],gpsSat[ci][cj]);
		fprintf(GpPipe,"\n"); // xgps format
//				fprintf(GpPipe,"%i %f %i",90-satLocAzi[gpsSat[ci][cj]],cos(deg2rad*satLocEle[gpsSat[ci][cj]]),satSNR[gpsSat[ci][cj]]);
//		fprintf(GpPipe,"\n");
			}
	}	
	fprintf(GpPipe,"e\n");
	for(ci=1;ci<=gpsPages;++ci)
	{
		for(cj=0;cj<gpsPerPage[ci];++cj)
			if( satSNR[gpsSat[ci][cj]]>0 )
			{
				fprintf(GpPipe,"%i %i %i %i",90-satLocAzi[gpsSat[ci][cj]],90-satLocEle[gpsSat[ci][cj]],satSNR[gpsSat[ci][cj]],gpsSat[ci][cj]);
		fprintf(GpPipe,"\n"); // xgps format
//				fprintf(GpPipe,"%i %f %i",90-satLocAzi[gpsSat[ci][cj]],cos(deg2rad*satLocEle[gpsSat[ci][cj]]),satSNR[gpsSat[ci][cj]]);
//		fprintf(GpPipe,"\n");
			}
	}	
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!
}


