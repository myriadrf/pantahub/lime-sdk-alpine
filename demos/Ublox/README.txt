Introdcution:

Welcome to Lime's Ublox demo, an example file using LimeNet micro Ublox module to decode GNSS data and to display its the NMEA messages.

Dependencies:

xgnuplot xdg-open limesuite

Set Up:

The following steps are required to set up.

sudo adduser -a -G dialout pi
sudo adduser -a -G tty pi
sudo raspi-config
# Disable serial login, enable serial interface 

sudo nano /boot/config.txt
# add the following lines at end of the file

# UART to UBLOX
gpio=32-33=a3
gpio=14-15=ip

reboot

To compile:

gcc ublox.c -lm -O3 -Wall -o ublox

To run:

sudo ./ublox "/dev/ttyAMA0"

To view results:

Click on GPS.html file.

