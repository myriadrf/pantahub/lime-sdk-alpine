/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// comment out to force test
#if __GNUC__ <6
	#define USE_C99_COMPLEX 1 // fftw_complex maps to C99 double _Complex (Ubuntu 16.04.5 gcc 5.4.0)
// #warning "GNU <=5"
#else // - see 4.1.1 of fftw3.pdf C99 compatibility with FFTW3 - only honoured in gcc 5?
	#define USE_FFTW_COMPLEX 2 // typedef double fftw_complex[2]; (Raspbian 6.0.3 gcc 6.3.0)
// #warning "GNU >=6"
#endif // Also see same problem on Ubuntu 18.04.1 with gcc 7.3.0.  Version 8.1.0 available

//#define USE_F32 0 // largest USB packets use only one of these!
#define USE_I16 1 // was I16, minimises computing processing USB packets
//#define USE_I12 2 // smallest USB packets




