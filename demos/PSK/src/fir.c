/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include <complex.h>
#include "psk.h"
#include "fir.h"

float h[HH];

void gauss( void )
{ // Gaussian FIR filter
	//char fname[25]="Gauss";
	int ct=0;
	for( ct=0; ct<HH; ++ct )
	{
		h[ct]=(ct-HH/2.0)/HH;
		h[ct]=exp( -25*h[ct]*h[ct] );
	}
	//GNUplotBuffer( fname,h,HH );
}

void fir( float veco[],float veci[] )
{ // circular FIR - in real life it is not!
	unsigned int ct=0;
	int ch=0;
	for( ct=0; ct<NOSR; ++ct )
		veco[ct]=0.0;
	for( ch=0; ch<HH; ++ch ) // convolution based FIR
		for( ct=0; ct<NOSR; ++ct )
			veco[ct]+=h[ch]*veci[(ct-ch+NOSR)%NOSR];
}

