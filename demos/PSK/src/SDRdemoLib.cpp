/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
//#include "LimeSuite.h"
#include "lime/LimeSuite.h"
#include "lime.h"
#include "SDRdemoLib.hpp"

using namespace std;

lms_device_t* device=NULL; // SDR device
lms_stream_t RXstreamId; // SDR stream
lms_stream_t TXstreamId; // SDR stream
lms_stream_meta_t rx_metadata;
lms_stream_meta_t tx_metadata;
unsigned char Ch=0; // only Channel 0 on LimeSDRmini
int delay=1024*32; // delay between RX data start time (before read), and TX data transmit
float _Complex *RXbuffer;
float _Complex *TXbuffer;
int bufferSize;

void StartSDR( double frq_cen,double frq_Samp,unsigned int RXgaindB,unsigned int TXgaindB,unsigned int pts )
{
	int n; //Find devices
	int ci=0;
	char LNAnum=2; // LNAL - use default
	unsigned char OSR=16;
	float_type rate=frq_Samp;
	float_type rf_rate=frq_Samp*OSR;
	bufferSize=pts;
	RXbuffer=(float _Complex *)malloc(sizeof(float _Complex)*bufferSize);
	TXbuffer=(float _Complex *)malloc(sizeof(float _Complex)*bufferSize);
	lms_name_t antenna_list[10]; //large enough list for antenna names.
	if((n=LMS_GetDeviceList(NULL))<0) // Pass NULL to only obtain number of devices
		error();
	printf("Devices found: %i \n",n);
	if(n<1)
		error();
	lms_info_str_t* list = new lms_info_str_t[n]; // allocate device list storage
	if(LMS_GetDeviceList(list)<0)
		error();
	for(ci=0;ci<n;ci++) // print device list
		printf("%i:%s\n",ci,list[ci]);
	if(LMS_Open(&device,list[0],NULL)) //Open the first device
		error();
	delete [] list;
	//Initialize device with default configuration
	//Use LMS_LoadConfig(device, "/path/to/file.ini") to load config from INI
	if(LMS_Init(device) != 0)
		error();
    if(LMS_EnableChannel(device,LMS_CH_RX,Ch,true)!=0) // Rx, Channels 0:(N-1)
		error();
	if(LMS_EnableChannel(device,LMS_CH_TX,Ch,true)!=0)
		error();
	if(LMS_SetLOFrequency(device,LMS_CH_RX,Ch,frq_cen)!=0)
		error();
	if(LMS_SetLOFrequency(device,LMS_CH_TX,Ch,frq_cen)!=0)
		error();
	//Alternatively, NULL can be passed to LMS_GetAntennaList() to obtain antennae num
	if((n = LMS_GetAntennaList(device, LMS_CH_RX, 0, antenna_list)) < 0)
		error();
	printf("Ae:\n"); //print available antennae names
	for(ci = 0; ci < n; ci++)
		printf(" %i:%s",ci,antenna_list[ci]);
	if((n = LMS_GetAntenna(device, LMS_CH_RX, Ch)) < 0) //get selected antenna index
		error();
	printf("\nDefault: %i:%s, ",n,antenna_list[n]);
	if( LNAnum==1 )
		if(LMS_SetAntenna(device, LMS_CH_RX, Ch, LMS_PATH_LNAH) != 0) 
			error();
	if( LNAnum==2 )
		if(LMS_SetAntenna(device, LMS_CH_RX, Ch, LMS_PATH_LNAL) != 0) 
			error();
	if( LNAnum==3 )
		if(LMS_SetAntenna(device, LMS_CH_RX, Ch, LMS_PATH_LNAW) != 0) 
			error();
	if((n = LMS_GetAntenna(device, LMS_CH_RX, Ch)) < 0) //get selected antenna index
		error();
	if(LMS_SetAntenna(device,LMS_CH_TX,Ch,LMS_PATH_TX1)!=0) 
		error();
	printf("Selected: %i:%s\n",n,antenna_list[n]);
    if(LMS_SetSampleRate(device,frq_Samp,OSR) != 0) // oversampling in RF OSR x sample rate
        error();
	if(LMS_GetSampleRate(device, LMS_CH_RX, 0, &rate, &rf_rate) != 0)  // can pass NULL 
		error();
	printf("\nUSB rate: %.3f MS/s, ADC rate: %.3fMS/s\n", rate*1.0e-6, rf_rate*1.0e-6);
    //Example of getting allowed parameter value range
    //There are also functions to get other parameter ranges (check LimeSuite.h)
	lms_range_t range; //Get allowed LPF bandwidth range
	if(LMS_GetLPFBWRange(device,LMS_CH_RX,&range)!=0)
		error(); // RX LPF range: 1.400100 - 130.000000 MHz
//	printf("RX LPF bandwitdh range: %f - %f MHz\n", range.min*1.0e-6,range.max*1.0e-6);   
	if(LMS_SetLPFBW(device, LMS_CH_RX, Ch, frq_Samp)!=0)  //Configure LPF, bandwidth 8 MHz
		error();
//	int err=0;
	if(LMS_SetGaindB(device,LMS_CH_RX,Ch,RXgaindB)!=0) // 0:73 breaks
	if(LMS_GetGaindB(device,LMS_CH_RX,Ch,&RXgaindB)!=0)
		error();
	if(LMS_SetGaindB(device,LMS_CH_TX,Ch,TXgaindB)!= 0) // 0:73
		error(); //tstLvl
	if(LMS_GetGaindB(device,LMS_CH_TX,Ch,&TXgaindB)!=0)
		error();
	printf("RX Gain: %i dB, TX Gain: %i dB\n",RXgaindB,TXgaindB);
//	if(LMS_SetTestSignal(device,LMS_CH_TX,Ch,LMS_TESTSIG_NCODIV8,0,0)!=0)
//		error(); // freq offset = rate/NCODIV
	if(LMS_SetLPFBW(device,LMS_CH_TX,Ch,30.0e6)!=0)
		error(); // TX LPF range: 1.400100 - 130.000000 MHz
	if(LMS_Calibrate(device,LMS_CH_RX,Ch,rate,0)!=0)
		error();
	if(LMS_Calibrate(device,LMS_CH_TX,Ch,rate,0)!=0) // 2.5 - 120MHz
		error();

    rx_metadata.flushPartialPacket = false; //currently has no effect in RX
    rx_metadata.waitForTimestamp = false; //currently has no effect in RX

    tx_metadata.flushPartialPacket = false; //do not force sending of incomplete packet
    tx_metadata.waitForTimestamp = true; //Enable synchronization to HW timestamp

	RXstreamId.channel=Ch; //channel number
	RXstreamId.fifoSize=4*pts; //fifo size in samples
	RXstreamId.throughputVsLatency=0.5; //optimize 0:1
	RXstreamId.isTx=false; //RX channel
#ifdef USE_F32
	RXstreamId.dataFmt=lms_stream_t::LMS_FMT_F32;
#endif
#ifdef USE_I16
	RXstreamId.dataFmt=lms_stream_t::LMS_FMT_I16;
#endif
#ifdef USE_I12
	RXstreamId.dataFmt=lms_stream_t::LMS_FMT_I12;
#endif

	TXstreamId.channel=Ch; //channel number
	TXstreamId.fifoSize=4*pts; //fifo size in samples
	TXstreamId.throughputVsLatency=0.5; //optimize 0:1
	TXstreamId.isTx=true; //TX channel
#ifdef USE_F32
	TXstreamId.dataFmt=lms_stream_t::LMS_FMT_F32;
#endif
#ifdef USE_I16
	TXstreamId.dataFmt=lms_stream_t::LMS_FMT_I16;
#endif
#ifdef USE_I12
	TXstreamId.dataFmt=lms_stream_t::LMS_FMT_I12;
#endif

	if(LMS_SetupStream(device,&RXstreamId)!=0)
		error();
	if(LMS_SetupStream(device,&TXstreamId)!=0)
		error();
	LMS_StartStream(&RXstreamId);
	LMS_StartStream(&TXstreamId);
}

void StopSDR( void )
{
	if(LMS_EnableChannel(device,LMS_CH_TX,Ch,false)!=0)
		error();
	if(LMS_SetGaindB(device,LMS_CH_TX,Ch,0)!= 0) // 0:73
		error(); // switch off Tx so not to interfere.
	LMS_StopStream(&RXstreamId); //stream is stopped, start again with LMS_StartStream()
	LMS_StopStream(&TXstreamId); //stream is stopped, start again with LMS_StartStream() 
	LMS_DestroyStream(device, &RXstreamId);//stream can no longer be used
	LMS_DestroyStream(device, &TXstreamId);//stream can no longer be used
	LMS_Close(device);
	free(RXbuffer);
	free(TXbuffer);
}

void TRX( int pts, int delay )
{ // 1000ms timeout
	int samplesRead;
	samplesRead=LMS_RecvStream(&RXstreamId,RXbuffer,pts,&rx_metadata,1000);
	tx_metadata.timestamp=rx_metadata.timestamp+delay;
	LMS_SendStream(&TXstreamId,TXbuffer,samplesRead,&tx_metadata,1000);
}

int error( void )
{
	printf("LimeSDR: ERROR\n");
	if(device!=NULL)
		LMS_Close(device);
	exit(-1);
}

