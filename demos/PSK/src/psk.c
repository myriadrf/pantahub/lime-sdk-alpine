/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include <complex.h>
#include "gnuPlotLib.h"
#include "psk.h"
#include "fir.h"

void mod( float _Complex *veco,signed char *veci )
{ 
	unsigned int ct=0;
	float vecUS[NOSR]; // upsampled version
	float vecFIR[NOSR]; // FIR filtered version
//	char fname1[25]="mod";
//	char fname2[25]="iq";
	for( ct=0; ct<NOSR; ++ct )
		vecUS[ct]=0.0;
	for( ct=0; ct<N*NRPT; ++ct )
		vecUS[OSR*ct]=veci[ct%N];
	fir( vecFIR,vecUS );
	for( ct=0; ct<NOSR; ++ct ) // PSK is a time dependent phase shift
		veco[ct]=cexpf(I*(PM*vecFIR[ct]));
//	GNUplotBuffer( fname1,vecFIR,NOSR );
//	GNUplotBufferConst( fname2,veco,NOSR );	
}

void demod( float veco[], float _Complex veci[] )
{
	char fname1[25]="demod";
//	char fname2[25]="iq";
//	char fname3[25]="phuw";
	float ph[NOSR]; // raw phase
	float phuw[NOSR]; // unwrapped phase
	float phacc=0.0;
	float phmin=100.0;
	float phmax=-100.0;
	float phoff=0.0;
	float rPM=1.0/PM;
	int ct=0;
	for( ct=0; ct<NOSR; ++ct ) 
		ph[ct]=cargf(veci[ct]);
	phuw[0]=ph[0];
	for( ct=1; ct<NOSR; ++ct ) // unwrap phase
	{
		phacc+=2*PI*(( (ph[ct-1]-ph[ct])>PHT )-( (ph[ct-1]-ph[ct])<-PHT ));
		phuw[ct]=ph[ct]+phacc;
		if(phuw[ct]<phmin)
			phmin=phuw[ct];
		if(phuw[ct]>phmax)
			phmax=phuw[ct];
	}
	phoff=0.5*(phmax+phmin); // center phase variation on 0
	for( ct=0; ct<NOSR; ++ct ) // vectorizes
		veco[ct]=rPM*(phuw[ct]-phoff);
	GNUplotBuffer( fname1,veco,NOSR );
//	GNUplotBuffer( fname3,phuw,NOSR );
//	GNUplotBufferConst( fname2,veci,NOSR );	// DC offset => amplitude variation
}

