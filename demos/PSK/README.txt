Introdcution:

Welcome to Lime's PSK demo, an example file using LimeSDR, LimeSDRmini and LimeNet micro to generate and receive PSK modulation.  
The PSK encoder uses a Gaussian FIR filter to contrain bandwidth and to minimise interference.
The SDR is tuned to 866MHz.  This is license exempt in UK if less than 25mW.

Dependecies:

libfftw3-3 xgnuplot xdg-open limesuite

To obtain limesuite, follow instructions on https://wiki.myriadrf.org/Lime_Suite
If you choose the repository version of LimeSuite, be sure to also include liblimesuite-dev 

To compile:

make clean
make

To run:

Connect RF cable directly from SDR TX output to RX input.
./PSK

Output graphs will be written into ../output


