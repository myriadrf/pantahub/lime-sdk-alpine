/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include "lime.h"
#ifdef USE_C99_COMPLEX
	#include <complex.h> // double _Complex, compliant with double, fftw_complex[2] Re=0, Im=1
	#include <fftw3.h>  // should come after complex.h
#else // #ifdef USE_FFTW_COMPLEX
	#include <fftw3.h>  // Do before complex.h to force typedef double fftw_complex[2]
	#include <complex.h> 
#endif
#include <unistd.h> // sleep pipes
#include "lime/LimeSuite.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "gnuPlotLib.h"
#include "fsk.h"
#include "fir.h"
#ifdef __cplusplus // If this is a C++ compiler, end C linkage
}
#endif
#include "SDRdemoLib.hpp"

using namespace std;

#ifdef USE_F32
extern float _Complex *TXbuffer;
extern float _Complex *RXbuffer;
#else // USE_I16  or USE_I12
extern int16_t *TXbuffer;
extern int16_t *RXbuffer;
#endif

int main( void )
{
	signed char  data[N]={1,-1,1,1, -1,-1,1,-1}; // bpsk input
	float _Complex iqDataTx[NOSR];
	float _Complex iqDataRx[NOSR];
	float data2[NOSR];
	unsigned int czzz=0;
	unsigned int ct=0;
	float R32767=1.0/32767;
	gauss();
	mod( iqDataTx, data );
	StartSDR( FRQ_CEN,FRQ_SAMP,RX_GAIN,TX_GAIN,1024*256 );
#ifdef USE_F32
	for(ct=0;ct<NOSR;ct++)
		TXbuffer[ct]=iqDataTx[ct];
#else
	for(ct=0;ct<NOSR;ct++)
	{
		TXbuffer[2*ct]  =32767*crealf(iqDataTx[ct]);
		TXbuffer[2*ct+1]=32767*cimagf(iqDataTx[ct]);
	}
#endif
	while( (++czzz)<20 ) // event loop
		TRX(PTS,DELAY); // repeat several times to flush bad packets and let PLLs settle
#ifdef USE_F32
	for(ct=0;ct<NOSR;ct++)
		iqDataRx[ct]=RXbuffer[ct];
#else // USE_I16 or USE_I12
	for(ct=0;ct<NOSR;ct++)
		iqDataRx[ct]=(RXbuffer[2*ct]+I*RXbuffer[2*ct+1])*R32767;
#endif
	StopSDR();
	demod( data2,iqDataRx ); // should be iqDataRx
	FFT3D( iqDataRx );
	return(0);
}
