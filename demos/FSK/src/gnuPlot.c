/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h> // double _Complex, compliant with double, fftw_complex[2] Re=0, Im=1
#include <unistd.h> // pipes usleep
#include <fcntl.h> // file control
#include <sys/types.h> // pipes
#include <sys/stat.h> // mkdir
#include <time.h> // tools for telling the time!
#include <errno.h> // EPERM etc

void GnuPlot3D( char *fName,double frq_cen,double frq_Samp,double tMax,float **z,int NFFT,int NSTEP )
{ // zz[NFRQ][NFFT], fName != fNameStem, as we have several sets of files to plot/print
	signed int ci,cj;
	FILE *GpPipe;
	int NFFT2=NFFT>>1; // NFFT/2
	double fscale=(frq_Samp/NFFT)*1e-6; // MHz
	double tStep=tMax/NSTEP;
	
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"/var/www/FSK/%s.gif\"\n",fName);
	fprintf(GpPipe,"set grid\n");
	fprintf(GpPipe,"set surface\n"); // not required?
//	fprintf(GpPipe,"set view 10,340\n");
//	fprintf(GpPipe,"set xrange [0:10]\n");
//	fprintf(GpPipe,"set yrange [0:10]\n");
//	fprintf(GpPipe,"set zrange [-90:0]\n");
	fprintf(GpPipe,"set pm3d\n"); // add map for heat map
	fprintf(GpPipe,"set xlabel \"FFT MHz (Cen %.3f MHz)\"\n",frq_cen);
	fprintf(GpPipe,"set ylabel \"Time ms\"\n");
	fprintf(GpPipe,"set xtics %f\n",frq_Samp*0.2e-6); // *1e-6/5
	fprintf(GpPipe,"set ytics %f\n",tMax*200); // *1e3/5
	fprintf(GpPipe,"set palette defined (-5 \"black\",-4 \"dark-green\",-3 \"forest-green\", -2 \"green\", -1 \"sea-green\", 0 \"cyan\", 1 \"blue\", 2 \"purple\", 3 \"pink\",4 \"red\", 5 \"orange\", 6 \"yellow\")\n"); // , 7 \"white\"
	fprintf(GpPipe,"set hidden3d\n");
	fprintf(GpPipe,"set contour base\n"); // base surface both
	fprintf(GpPipe,"splot '-' using 1:2:3 with pm3d title 'ch 1'\n");
	for(ci=0;ci<NSTEP;++ci)
	{ // 1-D x y z format
		for(cj=0;cj<NFFT;++cj)
			fprintf(GpPipe,"%f %f %f\n",(cj-NFFT2)*fscale,ci*tStep*1e3,z[ci][cj]);
		fprintf(GpPipe,"\n");
	}
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!	
}

void GNUplotBuffer( char *fname,float buf[],int len )
{
	int cj=0;
	FILE *GpPipe;
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"/var/www/FSK/%s.gif\"\n",fname);
//	fprintf(GpPipe,"set title \"%s\"\n",title);
//	fprintf(GpPipe,"set ylabel \"%s\"\n",yLab);
	fprintf(GpPipe,"set xlabel \"Samples\"\n"); // %s xLab
	// pt 4 open sq, pt 5 sq, pt 6 open circ, pt 7 circ pt, 12 open dia
	fprintf(GpPipe,"set style line 1 lt 2 lc rgb \"red\" lw 1 pt 12\n");
	fprintf(GpPipe,"set style line 2 lt 2 lc rgb \"green\" lw 1 pt 12\n");
//	fprintf(GpPipe,"set style line 3 lt 2 lc rgb \"blue\" lw 1 pt 12\n");
	fprintf(GpPipe,"set grid\n");
	fprintf(GpPipe,"plot '-' using 1:2 with lines ls 1 notitle\n");
	for(cj=0;cj<len;++cj)
		fprintf(GpPipe,"%i %f\n",cj,creal(buf[cj]));
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!	
}

void GNUplotBufferRI( char *fname,float _Complex buf[],int len )
{
	int cj=0;
	FILE *GpPipe;
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"/var/www/FSK/%s.gif\"\n",fname);
//	fprintf(GpPipe,"set title \"%s\"\n",title);
//	fprintf(GpPipe,"set ylabel \"%s\"\n",yLab);
	fprintf(GpPipe,"set xlabel \"Samples\"\n"); // %s xLab
	// pt 4 open sq, pt 5 sq, pt 6 open circ, pt 7 circ pt, 12 open dia
	fprintf(GpPipe,"set style line 1 lt 2 lc rgb \"red\" lw 1 pt 12\n");
	fprintf(GpPipe,"set style line 2 lt 2 lc rgb \"green\" lw 1 pt 12\n");
//	fprintf(GpPipe,"set style line 3 lt 2 lc rgb \"blue\" lw 1 pt 12\n");
	fprintf(GpPipe,"set grid\n");
	fprintf(GpPipe,"plot '-' using 1:2 with lines ls 1 notitle, '-' using 1:2 with lines ls 2 notitle,\n");
	for(cj=0;cj<len;++cj)
		fprintf(GpPipe,"%i %f\n",cj,creal(buf[cj]));
	fprintf(GpPipe,"e\n");
	for(cj=0;cj<len;++cj)
		fprintf(GpPipe,"%i %f\n",cj,cimag(buf[cj]));
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!	
}

void GNUplotBufferConst( char *fname,float _Complex buf[],int len )
{
	int cj=0;
	FILE *GpPipe;
	GpPipe=popen("gnuplot","w"); // init GnuPlot Pipe
	fprintf(GpPipe,"set term gif\n");
	fprintf(GpPipe,"set output \"/var/www/FSK/%s.gif\"\n",fname);
//	fprintf(GpPipe,"set title \"%s\"\n",title);
//	fprintf(GpPipe,"set ylabel \"%s\"\n",yLab);
//	fprintf(GpPipe,"set xlabel \"%s\"\n",xLab);
	// pt 4 open sq, pt 5 sq, pt 6 open circ, pt 7 circ pt, 12 open dia
	fprintf(GpPipe,"set style line 1 lt 2 lc rgb \"red\" lw 1 pt 12\n");
	fprintf(GpPipe,"set style line 2 lt 2 lc rgb \"green\" lw 1 pt 12\n");
//	fprintf(GpPipe,"set style line 3 lt 2 lc rgb \"blue\" lw 1 pt 12\n");
	fprintf(GpPipe,"set grid\n");
	fprintf(GpPipe,"plot '-' using 1:2 with lines ls 1 notitle,\n");
	for(cj=0;cj<len;++cj)
		fprintf(GpPipe,"%f %f\n",creal(buf[cj]),cimag(buf[cj]));
	fprintf(GpPipe,"e\n");
	fflush(GpPipe);
	pclose(GpPipe); // kill gnuplot process!	
}

