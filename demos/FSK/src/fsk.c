/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include <complex.h>
#include "gnuPlotLib.h"
#include "fsk.h"
#include "fir.h"

void mod( float _Complex *veco,signed char *veci )
{ // trapezoid integration = sum of average of incremental limits
	unsigned int ct=0;
	float vecUS[NOSR]; // upsampled version
	float vecFIR[NOSR]; // FIR filtered version
	float vecCT[NOSR]; // integrated version (FSK only)
//	char fname[25]="mod";
	for( ct=0; ct<NOSR; ++ct )
		vecUS[ct]=0.0;
	for( ct=0; ct<N*NRPT; ++ct )
		vecUS[OSR*ct]=veci[ct%N]; // upsample input
	fir( vecFIR,vecUS );
	vecCT[0]=0.0;
	for( ct=1; ct<NOSR; ++ct ) // cumulative trapezoid integration
		vecCT[ct]=vecCT[ct-1]+(vecFIR[ct]+vecFIR[ct-1])/2;
	for( ct=0; ct<NOSR; ++ct ) // FSK is a phase shift of the integrated modulation
		veco[ct]=cexpf(I*FM*vecCT[ct]);
//	GNUplotBuffer( fname,vecCT,NOSR );
}

void demod( float veco[], float _Complex veci[] )
{
	char fname1[25]="demod";
	char fname3[25]="phuw"; // phase unwrapped
	float ph[NOSR]; // raw phase
	float phuw[NOSR]; // unwrapped phase
	float phacc=0.0;
	int ct=0;
	for( ct=0; ct<NOSR; ++ct ) 
		ph[ct]=cargf(veci[ct]);
	phuw[0]=ph[0];
	for( ct=1; ct<NOSR; ++ct ) // unwrap phase
	{
		phacc+=2*PI*(( (ph[ct-1]-ph[ct])>PHT )-( (ph[ct-1]-ph[ct])<-PHT ));
		phuw[ct]=ph[ct]+phacc;
	}
	veco[0]=0.0;
	for( ct=1; ct<NOSR; ++ct ) // vectorizes
		veco[ct]=phuw[ct]-phuw[ct-1]; // differentiate phase
	GNUplotBuffer( fname1,veco,NOSR );
	GNUplotBuffer( fname3,phuw,NOSR );
}

