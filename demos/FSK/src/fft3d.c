/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include "lime.h"
#ifdef USE_C99_COMPLEX
	#include <complex.h> // double _Complex, compliant with double, fftw_complex[2] Re=0, Im=1
	#include <fftw3.h>  // should come after complex.h
#else // #ifdef USE_FFTW_COMPLEX
	#include <fftw3.h>  // Do before complex.h to force typedef double fftw_complex[2]
	#include <complex.h> 
#endif
#include <unistd.h> // sleep pipes
#include "lime/LimeSuite.h"
#include "gnuPlotLib.h"
#include "fsk.h"

void FFT3D( float _Complex *data )
{ // FFT vs Time type 3D graph using FFTW and GnuPlot
	char fname[50]="graf3d";
	int cj;
	short ct;
	short cf;
	short NSTEP=NOSR/NFFT;
	short NFFT2=NFFT>>1; // NFFT/2
	float RNFFTdB=-20*log10(NFFT); // dB20( 1/NFFT)
	float **z; // array for 3D graph
	float ffttmp[NFFT]; // temp vector for FFT result in C99 format
	float win[NFFT]; // windowing function for FFT
	double FFTBW=FRQ_SAMP;
	double TMAX=1.0/FFTBW*NSTEP;

#ifdef USE_C99_COMPLEX // fftw_malloc for double, fftwf_malloc for float
	double _Complex *in=(double _Complex *) fftw_malloc(sizeof(double _Complex)*NFFT); // align for SIMD
	double _Complex *out=(double _Complex *) fftw_malloc(sizeof(double _Complex)*NFFT); // align for SIMD
#else // #ifdef USE_FFTW_COMPLEX
	fftw_complex *in=(fftw_complex *) fftw_malloc(sizeof(fftw_complex)*NFFT); // align for SIMD
	fftw_complex *out=(fftw_complex *) fftw_malloc(sizeof(fftw_complex)*NFFT); // align for SIMD
#endif
	fftw_plan pfft; // FFTW plan
	pfft=fftw_plan_dft_1d(NFFT,in,out,FFTW_FORWARD,FFTW_ESTIMATE); // FFTW_ESTIMATE,FFTW_MEASURE

	printf("NOSR=%i NSTEP=%i NFFT2=%i FFTBW=%.3fMHz TMAX=%.3fms\n",NOSR,NSTEP,NFFT2,FFTBW*1e-6,TMAX*1e3);
	
	z=(float **)malloc(sizeof(float*)*NSTEP);
	for( cj=0;cj<NSTEP;cj++ )
		z[cj]=(float *)malloc(sizeof(float)*NFFT);
	for(ct=0;ct<NFFT;ct++) // higher order windows widen FFT response
//		win[ct]=1-cos((PI2*ct)/(NFFT-1)); // Hann
//		win[ct]=25.0/46.0-(1-25.0/46.0)*cos((PI2*ct)/(NFFT-1)); // Hamming
//		win[ct]=0.355768-0.487396*cos((PI2*ct)/(NFFT-1))+0.144232*cos((2*PI2*ct)/(NFFT-1))-0.012604*cos((3*PI2*ct)/(NFFT-1)); // Nutall
//		win[ct]=0.3635819-0.4891775*cos((PI2*ct)/(NFFT-1))+0.1365995*cos((2*PI2*ct)/(NFFT-1))-0.0106411*cos((3*PI2*ct)/(NFFT-1)); // Blackman Nutall
		win[ct]=0.35875-0.48829*cos((PI2*ct)/(NFFT-1))+0.14128*cos((2*PI2*ct)/(NFFT-1))-0.01168*cos((3*PI2*ct)/(NFFT-1)); // Blackman Harris

	for( cj=0;cj<NSTEP;cj++ )
	{
		#ifdef USE_C99_COMPLEX
		for( ct=0;ct<NFFT;ct++ ) // vectorized
			in[ct]=data[cj*NFFT+ct]*win[ct];
		#else // #ifdef USE_FFTW_COMPLEX
		for( ct=0;ct<NFFT;ct++ ) // vectorized
		{
			in[ct][0]=crealf(data[cj*NFFT+ct])*win[ct];
			in[ct][1]=cimagf(data[cj*NFFT+ct])*win[ct];
		}
		#endif
		fftw_execute(pfft);
		#ifdef USE_C99_COMPLEX // convert result to C99
		for( cf=0;cf<NFFT;cf++ ) // vectorized
			ffttmp[cf]=out[cf]; // convert to ||^2
		#else // #ifdef USE_FFTW_COMPLEX
		for( cf=0;cf<NFFT;cf++ ) // vectorized
			ffttmp[cf]=out[cf][0]+I*out[cf][1]; // convert to ||^2
		#endif
		for( cf=0; cf<NFFT2;cf++ )
		{
			z[cj][cf]=20*log10(cabsf(ffttmp[NFFT2+cf]))+RNFFTdB;
			z[cj][cf+NFFT2]=20*log10(cabsf(ffttmp[cf]))+RNFFTdB;
		}
	}
	GnuPlot3D( fname,FRQ_CEN*1e-6,FFTBW,TMAX,z,NFFT,NSTEP );
	for( cj=0;cj<NSTEP;cj++ )
		free(z[cj]);
	free(out);
	free(in);
}
