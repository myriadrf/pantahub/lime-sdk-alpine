/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#define FRQ_CEN 866e6 // 866Mhz License exempt band
#define FRQ_SAMP 4e6 // USB Sample rate - probably need to slow down for Pi
#define TX_GAIN 24 // was 24
#define RX_GAIN 48 // was 48
#define PI 3.14159265
#define PI2 (2*PI)
#define N 8 // number of original data symbols
#define NRPT 1 // repeat data
#define OSR 1024 // was 64
#define NFFT 64 // must be <= OSR
#define NOSR (NRPT*N*OSR)
#define PTS NOSR
#define FM (3.141592/8) // FM deviation was 8:32
#define PHT (3*PI/2) // detect phase flip when unwrapping, make robust to sample rate and noise
#define DELAY 1024*64

void gauss( void );
void mod( float _Complex *veco,signed char *veci );
void demod( float veco[], float _Complex veci[] );
void fir( float veco[], float veci[] );
void FFT3D( float _Complex *data );

