/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <math.h>
#include "lime.h"
#include <complex.h>
#include "gnuPlotLib.h"
#include "ask.h"
#include "fir.h"

void mod( float _Complex veco[], signed char veci[] )
{ // trap integration = sum of average of incremental limits
	int ct=0;
	float vecUS[NOSR]; // upsampled version
	float vecFIR[NOSR]; // FIR filtered version
//	char fname[25]="mod";
	for( ct=0; ct<NOSR; ++ct )
		vecUS[ct]=0.0;
	for( ct=0; ct<N*NRPT; ++ct )
		vecUS[OSR*ct]=veci[ct%N]; // upsample input
	fir( vecFIR,vecUS );
	for( ct=0; ct<NOSR; ++ct ) // vectorized
		veco[ct]=0.49+0.49*vecFIR[ct];
//	GNUplotBuffer( fname,vecFIR,NOSR );
}

void demod( float veco[], float _Complex veci[] )
{
	char fname1[25]="demod";
	int ct=0;
	float _Complex pk=0.0;
	for( ct=0; ct<NOSR; ++ct ) // find phase at pk amplitude
		if(cabs(veci[ct])>cabs(pk) )
			pk=veci[ct];
	pk=1.0/pk;
	for( ct=0; ct<NOSR; ++ct ) // vectorized
		veco[ct]=creal(veci[ct]*pk); // use if DC is lost - note polarity ambiguous
//		veco[ct]=cabs(veci[ct]*pk); // use if DC is present
	GNUplotBuffer( fname1,veco,NOSR );
}


