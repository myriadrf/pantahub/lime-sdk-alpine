/*
 Copyright 2018 Lime Microsystems Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

void GnuPlot3D( char *fName,double frq_cen,double frq_Samp,double tMax,float **z,int NFFT,int NSTEP );
void GNUplotBuffer( char *fname,float buf[],int len );
void GNUplotBufferRI( char *fname,float _Complex buf[],int len );

